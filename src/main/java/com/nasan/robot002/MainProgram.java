/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nasan.robot002;

import java.util.Scanner;

/**
 *
 * @author nasan
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot('x', 2, 2, map);
        Bomb bomb = new Bomb(5, 5);
        map.setRobot(robot);
        map.setBomb(bomb);
        while (true) {
            map.showMap();
            if(map.isBomb(robot.getX(),robot.getY())){
                break;
            }
            char direction = inputDirection(kb);
            if (direction == 'q') {
                System.out.println("ByeJaa");
                break;
            }
            robot.walk(direction);
        }
    }

    private static char inputDirection(Scanner kb) {
        char direction = kb.next().charAt(0);
        return direction;
    }
}
